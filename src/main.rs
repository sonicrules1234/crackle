//use ruma_identifiers::{OwnedRoomId, OwnedUserId};
//use serde::{Serialize, Deserialize};
use crackle_lib::{
    OwnedRoomAliasId, OwnedRoomId, OwnedRoomName, OwnedUserId, ServerName, StorageData, UserId,
    UserInfo,
};
use crossbeam_channel::{unbounded, TryRecvError};
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{
        disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen, SetTitle,
    },
};
use scanpw::scanpw;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::path::PathBuf;
//use std::convert::TryFrom;
//use ruma_client_api::sync;
use std::{
    error::Error,
    io::{self, Write},
};
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans, Text},
    widgets::{Block, Borders, List, ListItem, Paragraph},
    Frame, Terminal,
};
//use std::time::Duration;
//const TIMEOUT: Duration = Duration::from_secs(3);
use unicode_width::UnicodeWidthStr;
//use crossbeam_utils::atomic::AtomicCell;

//static message_box_height: AtomicCell<u16> = AtomicCell::new(3);

use clap::Parser;

/// TUI Matrix Client written in Rust
#[derive(Parser, Debug)]
#[clap(version, about)]
struct Args {
    /// Path to config file
    #[clap(short, long, parse(try_from_str), default_value = "config.yaml")]
    config: PathBuf,

    /// Generate blank config
    #[clap(short, long)]
    generate: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct CrackleConfig {
    matrix_url: String,
    auth_token: String,
    username: String,
    room: String,
}

enum Focus {
    MessageInput,
    JoinRoomInput,
    InitialSync,
    //RoomList,
    //PaticipantList,
    //History,
    Watching,
    //Login,
    //Register,
    //RoomSearch,
    RoomSelect,
}

fn find_top(bottom_index: usize, height: usize, num_rooms: usize) -> usize {
    if num_rooms < height - 1 {
        return 0;
    } else {
        return bottom_index - (height - 3);
    }
}

fn find_bottom(top_index: usize, height: usize, num_rooms: usize) -> usize {
    if top_index + (height - 3) >= num_rooms {
        if num_rooms == 0 {
            return 0;
        } else {
            return num_rooms - 1;
        }
    } else {
        return top_index + (height - 3);
    }
}
/// App holds the state of the application
struct App {
    /// Current value of the input box
    input: String,
    /// Current input mode
    focus: Focus,
    /// History of recorded messages
    messages: BTreeMap<OwnedRoomId, Vec<String>>,
    host: String,
    server_name: String,
    user_name: String,
    password: Option<String>,
    selected_room: Option<OwnedRoomId>,
    room: String,
    rooms: BTreeMap<OwnedRoomId, OwnedRoomName>,
    current_room: Option<OwnedRoomId>,
    room_id_to_canonical_alias: BTreeMap<OwnedRoomId, OwnedRoomAliasId>,
    auth_token: Option<String>,
    joined_rooms: Vec<OwnedRoomId>,
    room_to_direct_name: BTreeMap<OwnedRoomId, String>,
    room_to_aliases: BTreeMap<OwnedRoomId, Vec<OwnedRoomAliasId>>,
    room_user_id_list: BTreeMap<OwnedRoomId, Vec<OwnedUserId>>,
    room_userid: BTreeMap<OwnedUserId, UserInfo>,
    alias_to_room: BTreeMap<OwnedRoomAliasId, OwnedRoomId>,
    room_list_top: usize,
    room_list_height: usize,
    room_list_top_backup: usize,
    room_list_prev_height: usize,
}

impl App {
    pub async fn new(
        home_server_url: impl Into<String>,
        user_name: String,
        password: Option<String>,
        room: String,
        auth_token: Option<String>,
    ) -> Self {
        let mut hs_url = home_server_url.into();
        //let mut host: String;
        if !hs_url.starts_with("http://") && !hs_url.starts_with("https://") {
            hs_url = format!("https://{}", hs_url);
        }
        let hostparts = hs_url.split_once("://").unwrap();
        let server_name = hostparts
            .1
            .clone()
            .split(":")
            .map(|z| z.to_string())
            .collect::<Vec<String>>()[0]
            .clone();
        let mut good_username = user_name.clone();
        if good_username.starts_with("@") {
            good_username = good_username.split_once("@").unwrap().1.to_string();
            if good_username.contains(":") {
                good_username = good_username.split_once(":").unwrap().0.to_string();
            }
        }
        let username = match UserId::parse_with_server_name(
            good_username,
            &ServerName::parse(server_name.as_str()).unwrap(),
        ) {
            Ok(user_id) => user_id.localpart().to_string(),
            Err(_) => user_name.clone(),
        };
        Self {
            input: String::new(),
            focus: Focus::InitialSync,
            messages: BTreeMap::new(),
            host: hs_url,
            server_name: server_name,
            user_name: username,
            password: password,
            selected_room: None,
            current_room: None,
            rooms: BTreeMap::new(),
            room_id_to_canonical_alias: BTreeMap::new(),
            alias_to_room: BTreeMap::new(),
            room_to_aliases: BTreeMap::new(),
            joined_rooms: Vec::new(),
            room_user_id_list: BTreeMap::new(),
            room_userid: BTreeMap::new(),
            auth_token: auth_token,
            room: room,
            room_to_direct_name: BTreeMap::new(),
            room_list_top: 0,
            room_list_height: 2,
            room_list_top_backup: 0,
            room_list_prev_height: 2,
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let blank_config = include_str!("../config.yaml.dist");
    let cli_options = Args::parse();
    if cli_options.generate {
        let mut f = std::fs::File::create("config.yaml.dist").unwrap();
        f.write_all(blank_config.as_bytes()).unwrap();
        f.flush().unwrap();
        return Ok(());
    }
    let mut host: String;
    let mut username: String;
    let password: Option<String>;
    let auth_token: Option<String>;
    let mut room: String;
    if cli_options.config.exists() {
        let config: CrackleConfig =
            serde_yaml::from_str(&std::fs::read_to_string(cli_options.config).unwrap()).unwrap();
        host = config.matrix_url;
        username = config.username;
        auth_token = Some(config.auth_token);
        room = config.room;
        password = None;
    } else {
        host = String::new();
        println!("Matrix URL: ");
        std::io::stdin().read_line(&mut host).unwrap();
        username = String::new();
        println!("Username: ");
        std::io::stdin().read_line(&mut username).unwrap();
        password = Some(scanpw!("Password: ").trim().to_string());
        room = String::new();
        println!("\nRoom: ");
        std::io::stdin().read_line(&mut room).unwrap();
        auth_token = None;
    }
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(
        stdout,
        EnterAlternateScreen,
        EnableMouseCapture,
        SetTitle("Crackle")
    )?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let app = App::new(
        host.trim().to_string(),
        username.trim().to_string(),
        password,
        room.trim().to_string(),
        auth_token,
    )
    .await;
    let res = run_app(&mut terminal, app).await;

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err);
    }

    Ok(())
}
#[derive(Debug, Clone, Eq, PartialEq)]
enum SyncMessage {
    Quit,
    SendMessage(OwnedRoomId, String, String, bool),
    InitialSyncOver,
    /*
    RoomsUpdated(
        Vec<OwnedRoomId>,
        BTreeMap<OwnedRoomId, OwnedRoomName>,
        BTreeMap<OwnedRoomId, OwnedRoomAliasId>,
        BTreeMap<OwnedRoomId, String>,
        BTreeMap<OwnedRoomId, Vec<OwnedRoomAliasId>>,
        BTreeMap<OwnedRoomAliasId, OwnedRoomId>,
    ),
    */
    RoomsUpdated(StorageData),
    LeaveRoom(OwnedRoomId),
    JoinRoom(String),
}
//#[allow(unused_assignments)]
async fn run_app<B: Backend>(terminal: &mut Terminal<B>, mut app: App) -> Result<(), String> {
    let authentication: crackle_lib::Authentication;
    if let Some(pass) = app.password.clone() {
        /*
        if let Err(val) = client.login(pass) {
            return Err(format!("Error logging in: {:#?}", val));
        }
        */
        authentication = crackle_lib::Authentication::Password(pass);
    } else {
        //client.set_auth_token(app.auth_token.as_ref().unwrap().clone());
        authentication = crackle_lib::Authentication::AuthToken(app.auth_token.clone().unwrap());
    }
    let mut client = crackle_lib::Client::new(
        app.host.clone(),
        app.user_name.clone(),
        app.server_name.clone(),
        authentication,
    )
    .await;
    let me = client.get_me();
    let (tx1, rx1) = unbounded::<SyncMessage>();
    let tx1_clone = tx1.clone();
    let (tx2, rx2) = unbounded::<SyncMessage>();
    let sm_room_id = client.room_id_from_alias(app.room.clone()).await;
    let sm_room_id_clone = sm_room_id.clone();
    let networking = tokio::task::spawn(async move {
        let mut sync_results = client.sync().await.unwrap();
        client.extract_data(sync_results);
        tx1.send(SyncMessage::InitialSyncOver).unwrap();
        //let mut f = std::fs::File::create("sync.log").unwrap();
        //writeln!(f, "{:#?}", sync_results);
        if !client.data.joined_rooms.contains(&sm_room_id) {
            client.join_room_id(sm_room_id_clone).await;
        }
        loop {
            match rx2.try_recv() {
                Ok(SyncMessage::Quit) => break,
                Ok(SyncMessage::SendMessage(room_id, _user_id, message, _is_emote)) => {
                    client
                        .send_message(room_id.try_into().unwrap(), message)
                        .await;
                }
                Ok(SyncMessage::LeaveRoom(room_id)) => {
                    client.leave_room_id(room_id).await;
                }
                Ok(SyncMessage::JoinRoom(room)) => {
                    client.join_with_alias(room).await;
                }
                Ok(_) => {}
                Err(TryRecvError::Empty) => {}
                Err(_) => break,
            }
            if let Ok(sync_res) = client.sync().await {
                sync_results = sync_res;
            } else {
                continue;
            }
            let messages = client.extract_data(sync_results.clone()); //, client.clone().me);
            if client.room_list_updated() {
                tx1.send(SyncMessage::RoomsUpdated(
                    /*
                    client.data.joined_rooms.clone(),
                    client.data.room_to_name.clone(),
                    client.data.room_id_to_cananoical_alias.clone(),
                    client.data.room_to_direct_name.clone(),
                    client.data.room_to_aliases.clone(),
                    client.data.alias_to_room.clone(),
                    */
                    client.data.clone(),
                ))
                .unwrap();
            }
            for message in messages {
                let room_id = message.room_id;
                let user_id = message.sender.to_string();
                let msg = message.message.to_string();
                let is_emote = message.is_emote;
                tx1.send(SyncMessage::SendMessage(
                    room_id.clone(),
                    client.data.room_userid[&UserId::parse(user_id).unwrap()]
                        .get_name_string(room_id.clone()),
                    msg,
                    is_emote,
                ))
                .unwrap();
            }
        }
    });
    loop {
        terminal
            .draw(|f| ui(f, &mut app))
            .expect("Error drawing in terminal");
        if event::poll(std::time::Duration::from_secs(0)).unwrap() {
            if let Event::Key(key) = event::read().expect("Error reading event") {
                match app.focus {
                    Focus::RoomSelect => match key.code {
                        KeyCode::Up => {
                            if app.joined_rooms.len() > 1 {
                                let last = app.joined_rooms.iter().last().unwrap();
                                let first = app.joined_rooms.iter().nth(0).unwrap();
                                let mut prev = last.clone();
                                for (index_num, room_id) in app.joined_rooms.iter().enumerate() {
                                    if index_num == 0 {
                                        if first.clone() == app.selected_room.clone().unwrap() {
                                            app.selected_room = Some(last.clone());
                                            app.room_list_top = find_top(
                                                app.joined_rooms.len() - 1,
                                                app.room_list_height,
                                                app.joined_rooms.len(),
                                            );
                                            break;
                                        }
                                        prev = room_id.clone();
                                    } else {
                                        if room_id.clone() == app.selected_room.clone().unwrap() {
                                            if room_id.clone()
                                                == app.joined_rooms[app.room_list_top]
                                            {
                                                app.room_list_top -= 1;
                                            }
                                            app.selected_room = Some(prev);
                                            break;
                                        }
                                        prev = room_id.clone();
                                    }
                                }
                            }
                        }
                        KeyCode::Down => {
                            if app.joined_rooms.len() > 1 {
                                let last = app.joined_rooms.iter().last().unwrap();
                                let first = app.joined_rooms.iter().nth(0).unwrap();
                                let mut use_next = false;
                                for room_id in app.joined_rooms.iter() {
                                    if use_next {
                                        if app.selected_room.clone().unwrap()
                                            == app.joined_rooms[find_bottom(
                                                app.room_list_top,
                                                app.room_list_height,
                                                app.joined_rooms.len(),
                                            )]
                                        {
                                            app.room_list_top += 1;
                                        }
                                        app.selected_room = Some(room_id.clone());
                                        break;
                                    } else if room_id != last {
                                        if room_id.clone() == app.selected_room.clone().unwrap() {
                                            use_next = true;
                                        }
                                    } else {
                                        app.selected_room = Some(first.clone());
                                        app.room_list_top = 0;
                                    }
                                }
                            }
                        }
                        KeyCode::Char('l') => {
                            tx2.send(SyncMessage::LeaveRoom(app.selected_room.clone().unwrap()))
                                .unwrap();
                            app.selected_room = None;
                        }
                        KeyCode::Char('j') => {
                            app.focus = Focus::JoinRoomInput;
                        }
                        KeyCode::Enter => {
                            app.current_room = Some(app.selected_room.clone().unwrap());
                            app.selected_room = None;
                            app.focus = Focus::Watching;
                            app.room_list_top_backup = app.room_list_top;
                        }
                        KeyCode::Esc => {
                            app.selected_room = None;
                            app.focus = Focus::Watching;
                            app.room_list_top = app.room_list_top_backup;
                        }
                        _ => {}
                    },
                    Focus::Watching => match key.code {
                        KeyCode::Char('e') => {
                            app.focus = Focus::MessageInput;
                        }
                        KeyCode::Char('q') => {
                            tx2.send(SyncMessage::Quit).unwrap();
                            networking.abort();
                            return Ok(());
                        }
                        KeyCode::Char('r') => {
                            app.focus = Focus::RoomSelect;
                        }
                        _ => {}
                    },
                    Focus::MessageInput => match key.code {
                        KeyCode::Enter => {
                            let message: String = app.input.drain(..).collect();
                            if message != String::new() {
                                let sync_message = SyncMessage::SendMessage(
                                    app.current_room.clone().unwrap(),
                                    format!("{}", me),
                                    message,
                                    false,
                                );
                                tx1_clone.send(sync_message.clone()).unwrap();
                                tx2.send(sync_message.clone()).unwrap();
                            }
                        }
                        KeyCode::Char(c) => {
                            app.input.push(c);
                        }
                        KeyCode::Backspace => {
                            app.input.pop();
                        }
                        KeyCode::Esc => {
                            app.focus = Focus::Watching;
                        }
                        KeyCode::Tab => {
                            let mut words: Vec<String> = app
                                .input
                                .split(" ")
                                .collect::<Vec<&str>>()
                                .iter()
                                .map(|x| x.to_string())
                                .collect();
                            let current_word = words.iter().last().unwrap();
                            //let current_index = words.len() - 1;
                            let mut completions: Vec<&String> = app
                                .room_user_id_list
                                .get(app.current_room.as_ref().unwrap())
                                .unwrap()
                                .iter()
                                .filter_map(|x| {
                                    app.room_userid
                                        .get(x)
                                        .unwrap()
                                        .display_name_in_room
                                        .get(app.current_room.as_ref().unwrap())
                                })
                                .collect();
                            completions.retain(|x| {
                                x.to_lowercase().starts_with(&current_word.to_lowercase())
                            });
                            if completions.len() == 1 {
                                words.pop().unwrap();
                                words.push(completions[0].clone());
                                app.input = words.join(" ");
                            }
                        }
                        _ => {}
                    },
                    Focus::JoinRoomInput => match key.code {
                        KeyCode::Enter => {
                            let room: String = app.input.drain(..).collect();
                            let sync_message = SyncMessage::JoinRoom(room);
                            tx2.send(sync_message.clone()).unwrap();
                            app.focus = Focus::Watching;
                        }
                        KeyCode::Char(c) => {
                            app.input.push(c);
                        }
                        KeyCode::Backspace => {
                            app.input.pop();
                        }
                        KeyCode::Esc => {
                            app.focus = Focus::Watching;
                        }
                        _ => {}
                    },
                    Focus::InitialSync => {}
                }
            }
        } else {
            if let Ok(msg) = rx1.try_recv() {
                if let SyncMessage::SendMessage(room_id, user_id, message, is_emote) = msg {
                    let mut msgs = app.messages[&room_id].clone();
                    if is_emote {
                        msgs.push(format!("* {user_id} {message}"));
                    } else {
                        msgs.push(format!("<{}> {}", user_id, message));
                    }
                    if msgs.len() > 100 {
                        msgs.remove(0);
                    }
                    app.messages.insert(room_id.clone(), msgs);
                } else if msg == SyncMessage::InitialSyncOver {
                    app.focus = Focus::Watching;
                } else if let SyncMessage::RoomsUpdated(
                    /*
                    client.data.alias_to_room.clone(),
                    */
                    /*
                    alias_to_room,
                    */
                    data,
                ) = msg
                {
                    app.room_userid = data.room_userid;
                    app.room_user_id_list = data.room_id_user_list;
                    app.joined_rooms = data.joined_rooms.clone();
                    app.rooms = data.room_to_name.clone();
                    app.room_id_to_canonical_alias = data.room_id_to_cananoical_alias.clone();
                    app.room_to_direct_name = data.room_to_direct_name.clone();
                    app.room_to_aliases = data.room_to_aliases.clone();
                    app.alias_to_room = data.alias_to_room.clone();
                    for room_id in app.joined_rooms.clone() {
                        if !app.messages.contains_key(&room_id) {
                            app.messages.insert(room_id.clone(), Vec::new());
                        }
                    }
                }
            }
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        //.margin(2)
        .constraints(
            [
                Constraint::Length(1),
                Constraint::Min(1),
                Constraint::Length(3),
            ]
            .as_ref(),
        )
        .split(f.size());
    let middle_chunk = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(20), Constraint::Percentage(80)].as_ref())
        .split(chunks[1]);
    app.room_list_prev_height = app.room_list_height;
    app.room_list_height = middle_chunk[0].height as usize;
    let (msg, style) = match app.focus {
        Focus::Watching => (
            vec![
                Span::raw("Press "),
                Span::styled("q", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to exit, "),
                Span::styled("e", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to start editing, or "),
                Span::styled("r", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to choose a room "),
            ],
            Style::default().add_modifier(Modifier::RAPID_BLINK),
        ),
        Focus::MessageInput => (
            vec![
                Span::raw("Press "),
                Span::styled("Esc", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to stop editing, "),
                Span::styled("Enter", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to record the message"),
            ],
            Style::default(),
        ),
        Focus::JoinRoomInput => (
            vec![
                Span::raw("Press "),
                Span::styled("Esc", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to stop editing, "),
                Span::styled("Enter", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to join the room"),
            ],
            Style::default(),
        ),
        Focus::RoomSelect => (
            vec![
                Span::raw("Press "),
                Span::styled("Esc", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to stop choosing a room with the Up and Down arrow keys, "),
                Span::styled("Enter", Style::default().add_modifier(Modifier::BOLD)),
                Span::raw(" to view the selected room.  You can press l to leave the room or j to join a new one"),
            ],
            Style::default(),
        ),
        Focus::InitialSync => (
            vec![
                Span::raw("Performing inital sync.  Please wait..."),
            ],
            Style::default().add_modifier(Modifier::RAPID_BLINK),
        ),
    };
    let mut text = Text::from(Spans::from(msg));
    text.patch_style(style);
    let help_message = Paragraph::new(text);
    f.render_widget(help_message, chunks[0]);

    let input = Paragraph::new(app.input.as_ref())
        .style(match app.focus {
            Focus::Watching => Style::default(),
            Focus::MessageInput => Style::default().fg(Color::Yellow),
            Focus::RoomSelect => Style::default(),
            Focus::JoinRoomInput => Style::default().fg(Color::Yellow),
            Focus::InitialSync => Style::default(),
        })
        .block(Block::default().borders(Borders::ALL).title("Input"));
    match app.focus {
        Focus::MessageInput => {
            // Make the cursor visible and ask tui-rs to put it at the specified coordinates after rendering
            f.set_cursor(
                // Put cursor past the end of the input text
                chunks[2].x + app.input.width() as u16 + 1,
                // Move one line down, from the border to the input line
                chunks[2].y + 1,
            )
        }
        Focus::JoinRoomInput => {
            // Make the cursor visible and ask tui-rs to put it at the specified coordinates after rendering
            f.set_cursor(
                // Put cursor past the end of the input text
                chunks[2].x + app.input.width() as u16 + 1,
                // Move one line down, from the border to the input line
                chunks[2].y + 1,
            )
        }
        _ => {}
    }
    if app.joined_rooms.len() > 0 {
        if app.room_list_prev_height != app.room_list_height {
            let chosen: OwnedRoomId;
            if app.selected_room.is_none() {
                chosen = app.current_room.clone().unwrap();
            } else {
                chosen = app.selected_room.clone().unwrap();
            }
            let chosen_index = app.joined_rooms.iter().position(|y| &chosen == y).unwrap();
            if chosen_index
                > find_bottom(
                    app.room_list_top,
                    app.room_list_height,
                    app.joined_rooms.len(),
                )
            {
                app.room_list_top = chosen_index;
            } else if chosen_index < app.room_list_top {
                app.room_list_top =
                    find_top(chosen_index, app.room_list_height, app.joined_rooms.len());
            } else {
                app.room_list_top = find_top(
                    find_bottom(
                        app.room_list_top,
                        app.room_list_height,
                        app.joined_rooms.len(),
                    ),
                    app.room_list_height,
                    app.joined_rooms.len(),
                );
            }
        }
    }
    let room_list: Vec<ListItem> = app
        .joined_rooms
        .iter()
        .enumerate()
        .filter_map(|(index_num, room_id)| {
            if index_num < app.room_list_top
                || index_num
                    > find_bottom(
                        app.room_list_top,
                        app.room_list_height,
                        app.joined_rooms.len(),
                    )
            {
                return None;
            }
            let content: Vec<Spans>;
            if app.rooms.contains_key(room_id) {
                content = vec![Spans::from(Span::raw(format!("{}", app.rooms[room_id])))];
            } else if app.room_id_to_canonical_alias.contains_key(room_id) {
                content = vec![Spans::from(Span::raw(format!(
                    "{}",
                    app.room_id_to_canonical_alias[room_id]
                )))];
            } else if app.room_to_aliases.contains_key(room_id) {
                content = vec![Spans::from(Span::raw(format!(
                    "{}",
                    app.room_to_aliases[room_id][0]
                )))];
            } else if app.room_to_direct_name.contains_key(room_id) {
                content = vec![Spans::from(Span::raw(format!(
                    "{}",
                    app.room_to_direct_name[room_id]
                )))];
            } else {
                content = vec![Spans::from(Span::raw(format!("{}", room_id)))];
            }
            if app.current_room.is_none() {
                app.current_room = Some(room_id.clone());
            }
            let current_room = app.current_room.clone().unwrap();
            let mut z = ListItem::new(content);
            let mut style = Style::default();
            if room_id.clone() == current_room {
                style = style.add_modifier(Modifier::BOLD);
                z = z.style(style);
            }
            match app.focus {
                Focus::RoomSelect => {
                    if app.selected_room.is_none() {
                        app.selected_room = Some(current_room);
                    } else if app
                        .joined_rooms
                        .iter()
                        .position(|x| x.clone() == app.selected_room.clone().unwrap())
                        .unwrap()
                        < app.room_list_top
                    {
                        app.selected_room = Some(app.joined_rooms[app.room_list_top].clone());
                    } else if app
                        .joined_rooms
                        .iter()
                        .position(|x| x.clone() == app.selected_room.clone().unwrap())
                        .unwrap()
                        > find_bottom(
                            app.room_list_top,
                            app.room_list_height,
                            app.joined_rooms.len(),
                        )
                    {
                        app.selected_room = Some(
                            app.joined_rooms[find_bottom(
                                app.room_list_top,
                                app.room_list_height,
                                app.joined_rooms.len(),
                            )]
                            .clone(),
                        );
                    }
                    if app.selected_room.clone().unwrap() == room_id.clone() {
                        z = z.style(style.add_modifier(Modifier::REVERSED));
                    }
                }
                _ => {}
            }
            Some(z)
        })
        .collect();
    let room_list =
        List::new(room_list).block(Block::default().borders(Borders::ALL).title("Room List"));
    let mut msg_list: Vec<ListItem> = Vec::new();
    if app.current_room.is_some() {
        let ms = app.messages[&app.current_room.clone().unwrap()].join("\n");
        let lines: Vec<String> = textwrap::wrap(ms.as_str(), (middle_chunk[1].width - 2) as usize)
            .iter()
            .map(|x| x.to_string())
            .collect();
        let messages_len: i32 = lines.len().try_into().unwrap();
        let height: i32 = middle_chunk[1].height.try_into().unwrap(); //message_box_height.load().try_into().unwrap();
        let mut cut_off = messages_len - height + 2;
        if cut_off <= 0 {
            cut_off = 0;
        }
        msg_list = lines[cut_off.try_into().unwrap()..]
            .iter()
            .enumerate()
            .map(|(_i, m)| {
                let content = vec![Spans::from(Span::raw(format!("{}", m)))];
                ListItem::new(content)
            })
            .collect();
    }
    let messages =
        List::new(msg_list).block(Block::default().borders(Borders::ALL).title("Messages"));
    f.render_widget(messages, middle_chunk[1]);
    f.render_widget(input, chunks[2]);
    f.render_widget(room_list, middle_chunk[0]);
}
